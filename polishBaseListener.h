
// Generated from polish.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "polishListener.h"


/**
 * This class provides an empty implementation of polishListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  polishBaseListener : public polishListener {
public:

  virtual void enterStat(polishParser::StatContext * /*ctx*/) override { }
  virtual void exitStat(polishParser::StatContext * /*ctx*/) override { }

  virtual void enterDef(polishParser::DefContext * /*ctx*/) override { }
  virtual void exitDef(polishParser::DefContext * /*ctx*/) override { }

  virtual void enterExpr(polishParser::ExprContext * /*ctx*/) override { }
  virtual void exitExpr(polishParser::ExprContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

