
// Generated from polish.g4 by ANTLR 4.13.1


#include "polishListener.h"
#include "polishVisitor.h"

#include "polishParser.h"


using namespace antlrcpp;

using namespace antlr4;

namespace {

struct PolishParserStaticData final {
  PolishParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  PolishParserStaticData(const PolishParserStaticData&) = delete;
  PolishParserStaticData(PolishParserStaticData&&) = delete;
  PolishParserStaticData& operator=(const PolishParserStaticData&) = delete;
  PolishParserStaticData& operator=(PolishParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag polishParserOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
PolishParserStaticData *polishParserStaticData = nullptr;

void polishParserInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (polishParserStaticData != nullptr) {
    return;
  }
#else
  assert(polishParserStaticData == nullptr);
#endif
  auto staticData = std::make_unique<PolishParserStaticData>(
    std::vector<std::string>{
      "stat", "def", "expr"
    },
    std::vector<std::string>{
      "", "'='", "'+'", "'-'", "'*'", "'/'", "'^'", "'v'", "'s'", "'c'", 
      "'t'"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "", "OPE", "ID", "NUM", "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,14,51,2,0,7,0,2,1,7,1,2,2,7,2,1,0,1,0,1,0,1,0,1,0,1,0,3,0,13,8,0,
  	1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,
  	2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,3,2,
  	49,8,2,1,2,0,0,3,0,2,4,0,0,58,0,12,1,0,0,0,2,14,1,0,0,0,4,48,1,0,0,0,
  	6,7,3,4,2,0,7,8,5,0,0,1,8,13,1,0,0,0,9,10,3,2,1,0,10,11,5,0,0,1,11,13,
  	1,0,0,0,12,6,1,0,0,0,12,9,1,0,0,0,13,1,1,0,0,0,14,15,5,12,0,0,15,16,5,
  	1,0,0,16,17,3,4,2,0,17,3,1,0,0,0,18,49,5,13,0,0,19,20,5,2,0,0,20,21,3,
  	4,2,0,21,22,3,4,2,0,22,49,1,0,0,0,23,24,5,3,0,0,24,25,3,4,2,0,25,26,3,
  	4,2,0,26,49,1,0,0,0,27,28,5,4,0,0,28,29,3,4,2,0,29,30,3,4,2,0,30,49,1,
  	0,0,0,31,32,5,5,0,0,32,33,3,4,2,0,33,34,3,4,2,0,34,49,1,0,0,0,35,36,5,
  	6,0,0,36,37,3,4,2,0,37,38,3,4,2,0,38,49,1,0,0,0,39,40,5,7,0,0,40,49,3,
  	4,2,0,41,42,5,8,0,0,42,49,3,4,2,0,43,44,5,9,0,0,44,49,3,4,2,0,45,46,5,
  	10,0,0,46,49,3,4,2,0,47,49,5,12,0,0,48,18,1,0,0,0,48,19,1,0,0,0,48,23,
  	1,0,0,0,48,27,1,0,0,0,48,31,1,0,0,0,48,35,1,0,0,0,48,39,1,0,0,0,48,41,
  	1,0,0,0,48,43,1,0,0,0,48,45,1,0,0,0,48,47,1,0,0,0,49,5,1,0,0,0,2,12,48
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  polishParserStaticData = staticData.release();
}

}

polishParser::polishParser(TokenStream *input) : polishParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

polishParser::polishParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  polishParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *polishParserStaticData->atn, polishParserStaticData->decisionToDFA, polishParserStaticData->sharedContextCache, options);
}

polishParser::~polishParser() {
  delete _interpreter;
}

const atn::ATN& polishParser::getATN() const {
  return *polishParserStaticData->atn;
}

std::string polishParser::getGrammarFileName() const {
  return "polish.g4";
}

const std::vector<std::string>& polishParser::getRuleNames() const {
  return polishParserStaticData->ruleNames;
}

const dfa::Vocabulary& polishParser::getVocabulary() const {
  return polishParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView polishParser::getSerializedATN() const {
  return polishParserStaticData->serializedATN;
}


//----------------- StatContext ------------------------------------------------------------------

polishParser::StatContext::StatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

polishParser::ExprContext* polishParser::StatContext::expr() {
  return getRuleContext<polishParser::ExprContext>(0);
}

tree::TerminalNode* polishParser::StatContext::EOF() {
  return getToken(polishParser::EOF, 0);
}

polishParser::DefContext* polishParser::StatContext::def() {
  return getRuleContext<polishParser::DefContext>(0);
}


size_t polishParser::StatContext::getRuleIndex() const {
  return polishParser::RuleStat;
}

void polishParser::StatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStat(this);
}

void polishParser::StatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStat(this);
}


std::any polishParser::StatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<polishVisitor*>(visitor))
    return parserVisitor->visitStat(this);
  else
    return visitor->visitChildren(this);
}

polishParser::StatContext* polishParser::stat() {
  StatContext *_localctx = _tracker.createInstance<StatContext>(_ctx, getState());
  enterRule(_localctx, 0, polishParser::RuleStat);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(12);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(6);
      expr();
      setState(7);
      match(polishParser::EOF);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(9);
      def();
      setState(10);
      match(polishParser::EOF);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DefContext ------------------------------------------------------------------

polishParser::DefContext::DefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* polishParser::DefContext::ID() {
  return getToken(polishParser::ID, 0);
}

polishParser::ExprContext* polishParser::DefContext::expr() {
  return getRuleContext<polishParser::ExprContext>(0);
}


size_t polishParser::DefContext::getRuleIndex() const {
  return polishParser::RuleDef;
}

void polishParser::DefContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDef(this);
}

void polishParser::DefContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDef(this);
}


std::any polishParser::DefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<polishVisitor*>(visitor))
    return parserVisitor->visitDef(this);
  else
    return visitor->visitChildren(this);
}

polishParser::DefContext* polishParser::def() {
  DefContext *_localctx = _tracker.createInstance<DefContext>(_ctx, getState());
  enterRule(_localctx, 2, polishParser::RuleDef);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(14);
    match(polishParser::ID);
    setState(15);
    match(polishParser::T__0);
    setState(16);
    expr();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

polishParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* polishParser::ExprContext::NUM() {
  return getToken(polishParser::NUM, 0);
}

std::vector<polishParser::ExprContext *> polishParser::ExprContext::expr() {
  return getRuleContexts<polishParser::ExprContext>();
}

polishParser::ExprContext* polishParser::ExprContext::expr(size_t i) {
  return getRuleContext<polishParser::ExprContext>(i);
}

tree::TerminalNode* polishParser::ExprContext::ID() {
  return getToken(polishParser::ID, 0);
}


size_t polishParser::ExprContext::getRuleIndex() const {
  return polishParser::RuleExpr;
}

void polishParser::ExprContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpr(this);
}

void polishParser::ExprContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<polishListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpr(this);
}


std::any polishParser::ExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<polishVisitor*>(visitor))
    return parserVisitor->visitExpr(this);
  else
    return visitor->visitChildren(this);
}

polishParser::ExprContext* polishParser::expr() {
  ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, getState());
  enterRule(_localctx, 4, polishParser::RuleExpr);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(48);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case polishParser::NUM: {
        enterOuterAlt(_localctx, 1);
        setState(18);
        match(polishParser::NUM);
        break;
      }

      case polishParser::T__1: {
        enterOuterAlt(_localctx, 2);
        setState(19);
        match(polishParser::T__1);
        setState(20);
        expr();
        setState(21);
        expr();
        break;
      }

      case polishParser::T__2: {
        enterOuterAlt(_localctx, 3);
        setState(23);
        match(polishParser::T__2);
        setState(24);
        expr();
        setState(25);
        expr();
        break;
      }

      case polishParser::T__3: {
        enterOuterAlt(_localctx, 4);
        setState(27);
        match(polishParser::T__3);
        setState(28);
        expr();
        setState(29);
        expr();
        break;
      }

      case polishParser::T__4: {
        enterOuterAlt(_localctx, 5);
        setState(31);
        match(polishParser::T__4);
        setState(32);
        expr();
        setState(33);
        expr();
        break;
      }

      case polishParser::T__5: {
        enterOuterAlt(_localctx, 6);
        setState(35);
        match(polishParser::T__5);
        setState(36);
        expr();
        setState(37);
        expr();
        break;
      }

      case polishParser::T__6: {
        enterOuterAlt(_localctx, 7);
        setState(39);
        match(polishParser::T__6);
        setState(40);
        expr();
        break;
      }

      case polishParser::T__7: {
        enterOuterAlt(_localctx, 8);
        setState(41);
        match(polishParser::T__7);
        setState(42);
        expr();
        break;
      }

      case polishParser::T__8: {
        enterOuterAlt(_localctx, 9);
        setState(43);
        match(polishParser::T__8);
        setState(44);
        expr();
        break;
      }

      case polishParser::T__9: {
        enterOuterAlt(_localctx, 10);
        setState(45);
        match(polishParser::T__9);
        setState(46);
        expr();
        break;
      }

      case polishParser::ID: {
        enterOuterAlt(_localctx, 11);
        setState(47);
        match(polishParser::ID);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

void polishParser::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  polishParserInitialize();
#else
  ::antlr4::internal::call_once(polishParserOnceFlag, polishParserInitialize);
#endif
}
