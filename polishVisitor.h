
// Generated from polish.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "polishParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by polishParser.
 */
class  polishVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by polishParser.
   */
    virtual std::any visitStat(polishParser::StatContext *context) = 0;

    virtual std::any visitDef(polishParser::DefContext *context) = 0;

    virtual std::any visitExpr(polishParser::ExprContext *context) = 0;


};

