#include "polishLexer.h"
#include "polishParser.h"
#include "polishBaseVisitor.h"

#include <any>
#include <iostream>
#include <math.h>
#include <unordered_map>

std::unordered_map<std::string, double> memo;

class MyVisitor : public polishBaseVisitor {
public:
    antlrcpp::Any visitExpr(polishParser::ExprContext *ctx) override{
        // si el nodo visitado es una expresion
        if(ctx->children.size() == 3){ // si tiene 3 hijos
            // visitar hijos
            char op = ctx->children[0]->getText()[0];
            double left = std::any_cast<double>(visit(ctx->expr(0)));
            double right = std::any_cast<double>(visit(ctx->expr(1)));
                
            // realizar operaciones
            switch(op){
                case '+': return left + right;
                case '-': return left - right;
                case '*': return left * right;
                case '/': return left / right;
                case '^': return pow(left, right);
            }
        }else if(ctx->children.size() == 2){ // si tiene 2 hijos
            // visitar hijos
            char op = ctx->children[0]->getText()[0];
            double num = std::any_cast<double>(visit(ctx->expr(0)));

            // realizar operaciones
            switch(op){
                case 's': return sin(num);
                case 'c': return cos(num);
                case 't': return tan(num);
                case 'v': return sqrt(num);
            }
        }else{ //si solo tiene un hijo
            // castear para acceder al nodo
            auto terminalNode = dynamic_cast<antlr4::tree::TerminalNode*>(ctx->children[0]);
            // reibir el simbolo para saber que tipo de nodo es
            antlr4::Token* token = terminalNode->getSymbol();
            if(token->getType() == polishLexer::NUM){ // si es NUM
                // devolver numero
                return std::stod(ctx->NUM()->getText());
            }else{ // si es ID
                // acceder a la memoria y devolver el registro con ese nombre
                return memo[ctx->ID()->getText()]; 
            }
        }
        return 0;
    }

    antlrcpp::Any visitDef(polishParser::DefContext *ctx) override{
        memo[ctx->ID()->getText()] = std::any_cast<double>(visit(ctx->children[2]));
        return 0;
    }

    antlrcpp::Any visitStat(polishParser::StatContext *ctx) override{
        if(auto child = dynamic_cast<polishParser::ExprContext*>(ctx->children[0])){
            std::cout << std::any_cast<double>(visit(ctx->children[0])) << "\n";
        }else{
            visit(ctx->children[0]);
        }
        return 0;
    }
};

int main() {
    // Presentacion
    std::cout << "Polish Calculator Console\n";
    std::cout << "Type 'exit' to quit.\n\n";
    
    // Instanciar el visitador
    MyVisitor visitor;

    while (true) {
        // Recibir el input
        std::cout << ">>> ";
        std::string input;
        getline(std::cin, input);
        
        // Condicion para acabar
        if (input == "exit") {
            break;
        }

        // Crear input stream de ANTLR
        antlr4::ANTLRInputStream input_stream(input);

        // Crear lexer
        polishLexer lexer(&input_stream);
        antlr4::CommonTokenStream tokens(&lexer);

        // Crear parser
        polishParser parser(&tokens);

        // Parsear el input para obtener el arbol
        polishParser::StatContext* tree = parser.stat();

        // Visitar el arbol con visitador custom 
        visitor.visit(tree);
    }

    return 0;
}
