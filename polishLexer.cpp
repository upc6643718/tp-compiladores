
// Generated from polish.g4 by ANTLR 4.13.1


#include "polishLexer.h"


using namespace antlr4;



using namespace antlr4;

namespace {

struct PolishLexerStaticData final {
  PolishLexerStaticData(std::vector<std::string> ruleNames,
                          std::vector<std::string> channelNames,
                          std::vector<std::string> modeNames,
                          std::vector<std::string> literalNames,
                          std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), channelNames(std::move(channelNames)),
        modeNames(std::move(modeNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  PolishLexerStaticData(const PolishLexerStaticData&) = delete;
  PolishLexerStaticData(PolishLexerStaticData&&) = delete;
  PolishLexerStaticData& operator=(const PolishLexerStaticData&) = delete;
  PolishLexerStaticData& operator=(PolishLexerStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> channelNames;
  const std::vector<std::string> modeNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag polishlexerLexerOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
PolishLexerStaticData *polishlexerLexerStaticData = nullptr;

void polishlexerLexerInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (polishlexerLexerStaticData != nullptr) {
    return;
  }
#else
  assert(polishlexerLexerStaticData == nullptr);
#endif
  auto staticData = std::make_unique<PolishLexerStaticData>(
    std::vector<std::string>{
      "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
      "T__9", "OPE", "ID", "NUM", "WS"
    },
    std::vector<std::string>{
      "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    },
    std::vector<std::string>{
      "DEFAULT_MODE"
    },
    std::vector<std::string>{
      "", "'='", "'+'", "'-'", "'*'", "'/'", "'^'", "'v'", "'s'", "'c'", 
      "'t'"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "", "OPE", "ID", "NUM", "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,0,14,78,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
  	6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,1,0,
  	1,0,1,1,1,1,1,2,1,2,1,3,1,3,1,4,1,4,1,5,1,5,1,6,1,6,1,7,1,7,1,8,1,8,1,
  	9,1,9,1,10,1,10,1,11,1,11,5,11,54,8,11,10,11,12,11,57,9,11,1,12,4,12,
  	60,8,12,11,12,12,12,61,1,12,1,12,4,12,66,8,12,11,12,12,12,67,3,12,70,
  	8,12,1,13,4,13,73,8,13,11,13,12,13,74,1,13,1,13,0,0,14,1,1,3,2,5,3,7,
  	4,9,5,11,6,13,7,15,8,17,9,19,10,21,11,23,12,25,13,27,14,1,0,5,8,0,42,
  	43,45,45,47,47,61,61,94,94,99,99,115,116,118,118,3,0,65,90,95,95,97,122,
  	4,0,48,57,65,90,95,95,97,122,1,0,48,57,3,0,9,10,12,13,32,32,82,0,1,1,
  	0,0,0,0,3,1,0,0,0,0,5,1,0,0,0,0,7,1,0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,0,
  	13,1,0,0,0,0,15,1,0,0,0,0,17,1,0,0,0,0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,
  	0,0,0,0,25,1,0,0,0,0,27,1,0,0,0,1,29,1,0,0,0,3,31,1,0,0,0,5,33,1,0,0,
  	0,7,35,1,0,0,0,9,37,1,0,0,0,11,39,1,0,0,0,13,41,1,0,0,0,15,43,1,0,0,0,
  	17,45,1,0,0,0,19,47,1,0,0,0,21,49,1,0,0,0,23,51,1,0,0,0,25,59,1,0,0,0,
  	27,72,1,0,0,0,29,30,5,61,0,0,30,2,1,0,0,0,31,32,5,43,0,0,32,4,1,0,0,0,
  	33,34,5,45,0,0,34,6,1,0,0,0,35,36,5,42,0,0,36,8,1,0,0,0,37,38,5,47,0,
  	0,38,10,1,0,0,0,39,40,5,94,0,0,40,12,1,0,0,0,41,42,5,118,0,0,42,14,1,
  	0,0,0,43,44,5,115,0,0,44,16,1,0,0,0,45,46,5,99,0,0,46,18,1,0,0,0,47,48,
  	5,116,0,0,48,20,1,0,0,0,49,50,7,0,0,0,50,22,1,0,0,0,51,55,7,1,0,0,52,
  	54,7,2,0,0,53,52,1,0,0,0,54,57,1,0,0,0,55,53,1,0,0,0,55,56,1,0,0,0,56,
  	24,1,0,0,0,57,55,1,0,0,0,58,60,7,3,0,0,59,58,1,0,0,0,60,61,1,0,0,0,61,
  	59,1,0,0,0,61,62,1,0,0,0,62,69,1,0,0,0,63,65,5,46,0,0,64,66,7,3,0,0,65,
  	64,1,0,0,0,66,67,1,0,0,0,67,65,1,0,0,0,67,68,1,0,0,0,68,70,1,0,0,0,69,
  	63,1,0,0,0,69,70,1,0,0,0,70,26,1,0,0,0,71,73,7,4,0,0,72,71,1,0,0,0,73,
  	74,1,0,0,0,74,72,1,0,0,0,74,75,1,0,0,0,75,76,1,0,0,0,76,77,6,13,0,0,77,
  	28,1,0,0,0,6,0,55,61,67,69,74,1,6,0,0
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  polishlexerLexerStaticData = staticData.release();
}

}

polishLexer::polishLexer(CharStream *input) : Lexer(input) {
  polishLexer::initialize();
  _interpreter = new atn::LexerATNSimulator(this, *polishlexerLexerStaticData->atn, polishlexerLexerStaticData->decisionToDFA, polishlexerLexerStaticData->sharedContextCache);
}

polishLexer::~polishLexer() {
  delete _interpreter;
}

std::string polishLexer::getGrammarFileName() const {
  return "polish.g4";
}

const std::vector<std::string>& polishLexer::getRuleNames() const {
  return polishlexerLexerStaticData->ruleNames;
}

const std::vector<std::string>& polishLexer::getChannelNames() const {
  return polishlexerLexerStaticData->channelNames;
}

const std::vector<std::string>& polishLexer::getModeNames() const {
  return polishlexerLexerStaticData->modeNames;
}

const dfa::Vocabulary& polishLexer::getVocabulary() const {
  return polishlexerLexerStaticData->vocabulary;
}

antlr4::atn::SerializedATNView polishLexer::getSerializedATN() const {
  return polishlexerLexerStaticData->serializedATN;
}

const atn::ATN& polishLexer::getATN() const {
  return *polishlexerLexerStaticData->atn;
}




void polishLexer::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  polishlexerLexerInitialize();
#else
  ::antlr4::internal::call_once(polishlexerLexerOnceFlag, polishlexerLexerInitialize);
#endif
}
