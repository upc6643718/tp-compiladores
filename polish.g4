grammar polish;

OPE: [=+\-*/^vsct];

ID: [a-zA-Z_][a-zA-Z_0-9]* ;

NUM: [0-9]+('.'[0-9]+)?;
WS: [ \t\n\r\f]+ -> skip ;

stat
    : expr EOF
    | def EOF
    ;
    
def : ID '=' expr;

expr: NUM
    | '+' expr expr 
    | '-' expr expr
    | '*' expr expr
    | '/' expr expr
    | '^' expr expr
    | 'v' expr
    | 's' expr
    | 'c' expr
    | 't' expr
    | ID
    ;
