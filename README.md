# Trabajo Parcial - Grupo 1
### Integrantes
Marcelo Poggi
Claudia Sifuentes
Jorge Tarapa


## Introducción
El presente trabajo se centra en la implementación de una calculadora en notación polaca utilizando ANTLR4 en el lenguaje de programación C++. La notación polaca es un método de notación matemática en la que los operadores siguen a sus operandos, lo que facilita su evaluación por parte de un programa.

## Problematica
Este proyecto tiene como objetivo desarrollar una calculadora que pueda realizar evaluaciones precisas y eficientes de expresiones matemáticas escritas en notación polaca. La principal motivación detrás de esta iniciativa es la necesidad de contar con una herramienta práctica y fácilmente utilizada para realizar cálculos matemáticos en el contexto de la programación.

## Objetivos
* Implementar un analizador léxico que reconozca números reales en formato C++.
* Utilizar al menos cinco funciones adicionales de la biblioteca math.h para ampliar las capacidades de la calculadora.
* Considerar el manejo de variables, asignaciones y uso dentro de las expresiones evaluadas.
* Desarrollar el proyecto utilizando un repositorio de GitLab para gestionar el código fuente y facilitar la colaboración.

## Marco Teorico
La notación polaca inversa, también denominada notación prefija, fue presentada por el lógico polaco Jan Łukasiewicz en 1924. En este enfoque, los operadores preceden a sus operandos, lo que elimina la necesidad de utilizar paréntesis y simplifica la evaluación de expresiones matemáticas de forma computacional.

ANTLR4 es una herramienta especializada en la creación de analizadores léxicos y sintácticos, diseñada para facilitar el desarrollo de intérpretes y compiladores. Ofrece una sintaxis intuitiva para definir gramáticas de lenguaje y tiene la capacidad de generar código fuente en múltiples lenguajes de programación, incluyendo C++.

## Desarrollo
El desarrollo del proyecto implica la implementación de un analizador léxico y sintáctico utilizando ANTLR4, que será capaz de reconocer expresiones en notación polaca y evaluarlas correctamente. Se utilizará la biblioteca math.h para incorporar funciones matemáticas adicionales, y se implementará la manipulación de variables dentro de las expresiones evaluadas.

### Expresiones Regulares y Gramática
Se definirán expresiones regulares y reglas gramaticales en ANTLR4 para reconocer y analizar las diferentes partes de una expresión en notación polaca. Esto incluirá la definición de tokens para números, identificadores (variables), operadores y otros elementos relevantes.

#### Lexer

```
# Declaración del nombre de nuestra gramática
grammar polish;

# Operadores y funciones matematicas
OPE: [=+\-*/^vsct]; 

# Identificadores de variables
ID: [a-zA-Z_][a-zA-Z_0-9]* ; 

# numeros enteros o decimales
NUM: [0-9]+('.'[0-9]+)?; 

# white spaces
WS: [ \t\n\r\f]+ -> skip ; 
```
#### Parser
```
# Definimos la estructura de una instrucción en el lenguaje
stat
    : expr EOF
    | def EOF
    ;
 
# Definimos la asignación de variables
def : ID '=' expr;

# Define las expresiones matemáticas permitidas
expr: NUM
    | '+' expr expr 
    | '-' expr expr
    | '*' expr expr
    | '/' expr expr
    | '^' expr expr
    | 'v' expr
    | 's' expr
    | 'c' expr
    | 't' expr
    | ID
    ;
```
### Estructura de la Tabla de Símbolos
Se utilizará una tabla de símbolos implementada como un unordered_map en C++ para almacenar variables definidas por el usuario y sus valores correspondientes. Esto permitirá el manejo adecuado de variables dentro de las expresiones evaluadas.

#### Inicialización de la tabla
![image](https://hackmd.io/_uploads/H1-aCjWGR.png)
* Inicialización: Al inicio del programa, la tabla de símbolos se inicializa vacía para prepararse para la definición y almacenamiento de variables.
* Preparación para almacenamiento: La tabla de símbolos vacía proporciona un espacio listo para almacenar nombres de variables y sus valores asociados a medida que se definan.

#### Manejo de definiciones de variables
![image](https://hackmd.io/_uploads/HymJAiZfC.png)

Esta función se encarga de manejar la visita de nodos en la gramática definida por ANTLR4 cuando se encuentra una declaración de variable y su valor asignado. Extrae el nombre de la variable y evalúa la expresión que define su valor, luego almacena esta información en la tabla de símbolos memo. 

### Pruebas
A continuación, demostraremos el funcionamiento correcto de nuestra calculadora en notación polaca. Se presentarán diversos casos de uso para ilustrar su capacidad de evaluar expresiones matemáticas de manera precisa y eficiente. Además, se mostrará cómo nuestra calculadora puede manejar variables, permitiendo asignar valores y realizar cálculos con ellas

![image](https://hackmd.io/_uploads/Syw1P3ZMA.png)

## Conclusiones
El desarrollo de esta calculadora en notación polaca inversa con ANTLR4 en C++ proporciona una herramienta poderosa y flexible para realizar cálculos matemáticos de manera eficiente. La combinación de la notación polaca inversa y las capacidades de ANTLR4 facilita la implementación de un intérprete robusto y versátil. La utilización de un repositorio de GitLab para gestionar el código fuente garantiza la colaboración y el control de versiones de manera efectiva.

