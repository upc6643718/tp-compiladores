
// Generated from polish.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "polishVisitor.h"


/**
 * This class provides an empty implementation of polishVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  polishBaseVisitor : public polishVisitor {
public:

  virtual std::any visitStat(polishParser::StatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitDef(polishParser::DefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitExpr(polishParser::ExprContext *ctx) override {
    return visitChildren(ctx);
  }


};

