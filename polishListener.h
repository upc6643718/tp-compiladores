
// Generated from polish.g4 by ANTLR 4.13.1

#pragma once


#include "antlr4-runtime.h"
#include "polishParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by polishParser.
 */
class  polishListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterStat(polishParser::StatContext *ctx) = 0;
  virtual void exitStat(polishParser::StatContext *ctx) = 0;

  virtual void enterDef(polishParser::DefContext *ctx) = 0;
  virtual void exitDef(polishParser::DefContext *ctx) = 0;

  virtual void enterExpr(polishParser::ExprContext *ctx) = 0;
  virtual void exitExpr(polishParser::ExprContext *ctx) = 0;


};

